﻿# Curso de Synology

## Administración de permisos

Para poder administrar los permisos es necesario saber con qué permisos contamos en este caso podemos ver que contamos con permisos de lectura, escritura y Administración, los cuales se le pueden aplicar a los usuarios.
| Lectura | Escritura | Administración |
|--|--|--|
| Recorrer las carpetas/Ejecutar Archivos	 | Crear Archivos/Escribir datos | Cambiar Permisos |
| Lista de carpetas/Lectura de datos | Crear carpetas/anexar datos | Tomar Posesión |
| Leer atributos	 | Escribir atributos | - - - - |
| Leer atributos extendidos | Escribir atributos extendidos	 | - - - - |
| Leer permisos | Eliminar subcarpetas y archivos	 | - - - - | 
| - - - -| Eliminar | - - - - |

Para poder entender mejor estos permisos es necesario saber que hace y en que afecta cada uno de estos permisos al usuario final, a continuación se detalla qué es lo que hace cada permiso.

Administración:

* Cambiar permisos: controla si un usuario puede modificar los permisos del archivo o la carpeta.
* Toma de posesión: controla si un usuario tiene la propiedad del archivo o la carpeta.

Lectura:

* Recorrer las carpetas/Ejecutar archivos: controla si un usuario puede ejecutar un archivo de programa.
* Lista de carpetas/Lectura de datos: controla si un usuario puede leer datos en un archivo.
* Leer atributos: controla si un usuario puede ver los atributos de un archivo.
* Leer atributos extendidos: controla si un usuario puede ver los atributos ampliados de un archivo.
* Leer permisos: controla si un usuario puede leer los permisos del archivo o la carpeta.

Escritura:

* Crear archivos/Escribir datos: controla si un usuario puede modificar el contenido de un archivo.
* Crear carpetas/anexar datos: controla si un usuario puede añadir datos al final de un archivo.
* Escribir atributos: controla si un usuario puede modificar los atributos de un archivo.
* Escribir atributos extendidos: controla si un usuario puede modificar los atributos ampliados de un archivo.
* Eliminar subcarpetas y archivos: controla si un usuario puede borrar una carpeta.
* Eliminar: controla si un usuario puede eliminar un archivo.

Una vez sabiendo para que sirve cada permiso podemos seguir con la creación del usuario

----

## Creación de usuario

Para poder hacer la creacion de los usuarios es necesario ingresar a nuestra NAS para esto podemos acceder a traves de IP o de Un nombre de dominio, en nuestro caso vamos a ingresar a traves de ip la cual es [192.168.100.35:5000](192.168.100.35:5000) una vez dentro nos vamos al panel de control y seleccionaremos el apartado de usuario.


![Panel de Control](media/Panel%20de%20control.png)


Una vez dentro del panel de control le vamos a dar en el boton de crear y nos mandara la siguiente vista de un formulario.

![](media/Formulario.png)

Este Formulario lo tendremos que llenar con los datos solicitados en nuestro caso es con el usuario 'Esteban Q', en caso de requerir que se envie una notificacion por correo lo unico que se tiene que hacer es ingresar el correo y activar el servicio SMTP de la NAS para qie este pueda funcionar.

![](media/llenado%20de%20formulario.png)

Ya que llenamos nuestro formulario le daremos en siguiente y lo proximo que nos pedira sera asignar un grupo en este caso al grupo que se asignara sera al de users o usuarios, cabe resalta que existen otros dos grupos, el de adminitradores permite accesar a funciones mas espeficicas para poder llevar un control sobre la nas y el de http el cual es para los servicios web.

![](media/asignacion%20de%20grupo.png)

Le vamos a asignar el permiso a las carpeta o carpetas que el usuario podra ver en este caso le damos los permisos de lectura y escritura sobre la carpeta "usbshare1"

![](media/Asginacion%20de%20permisos%20de%20carpetas.png)

Seguido de esto tendremos que darle que cantidad de informacion puede transferir, a esta accion se le llama quota, si no queremos ponerle un limite de transferencia simplemente se deja en cero la quota.

![](media/quota.png)


Lo que sigue es un paso crucial para que el usuario final pueda consultar esta informacion alojada en la nas y es la forma en como va accesar dentro de las opciones que tenemos es atraves de

* FTP
* File Station
* Network Bacup
* WebDav
* Synology Drive (Esta se habilita haciendo otra configuracion por a parte)

En nuestro caso por el momento le vamos a dar acceso a traves de "File Station" para que pueda acceder via web y pueda consultar la información.

![](media/Permisos%20de%20aplicacion.png)

Para finalizar este apartado tenemos que confirmar todos los ajustes antes aplicados, por lo que nos saldra la siguiente ventana en la que tenemos que revisar que la informacion que hayamos escogido es la correcta, lo cual en este caso si lo es, si esto no fuera correcto aun estas a tiempo de poder escoger la configuración correcta.

![](media/Finalizacion.png)

----


## Creación de usuarios de forma masiva



Para poder hacer la creacion de usuario de forma masiva es necesario saber que es lo que vamos a ingresar y con que formato debe contar el archivo para que este pueda ser leido por la nas. 
El archivo debe ser creado en UTF-8 en un formato de txt, la informacion que debe contener el archivo es nombre, contraseña, Descripcion, correo electronico y cuota. La separacion de estos datos esta comprendida por un tabulador entre cada dato, un ejemplo seria el [siguiente](https://gitlab.com/MU5741N3/synology/-/blob/master/UserNas.txt).

Una vez hecho esto vamos a importar los usuarios y vamos a seleccionar el archivo que acabamos de crear, al momento de seleccionarlo y aceptarlo la nas nos regresara la siguiente pantalla.

![](media/massive%201.png)

Revisaremos que la informacion sea la correcta y le daremos aceptar, una vez hecho nos regresara un aviso en el cual veremos que se han creado de forma exitosa los usuarios.

![](media/massive%202.png)

ya solo quedaria revisar que los usuarios si estan creados y eso lo podemos verificar en la parte de usuarios dentro del panel de control.

![](media/massive%203.png)

---

## Crear Carpetas compartidas

Para poder crear las carpetas compartidas es necesario que vayamos a panel de control, vamos a ir a crear carpeta compartida y llenaremos la informacion que nos solicitan en este caso se llamara Musica.

![](media/shared%201.png)

Nota. Recuerda siempre habilitar la opcion de papelera de reciclaje y ocultar esta carpeta en mis ubicaciones de red esto para poder tener un control de los archivos.

Una vez que se haya creado esta carpeta vamos a asignarle que usuarios pueden visualizar dicha carpeta y bajo que permisos lo pueden tener, en este caso todos los usuarios tendran lectura y escritura dentro de la carpeta.

![](media/shared%202.png)

le damos en aceptar y ya con esto veremos que ya esta nuestra carpeta lista para poder ser usada por los usuarios.

---


## Permisos heredados en Subcarpetas

ya que tenemos creado nuestro recurso compartido es muy posible que dentro de este haya carpetas las cuales no quieres que vean ciertos usuarios para esto vamos a crear una carpeta dentro de nuestro recurso compartido.
Una vez creada esa carpeta tendremos que ver que permisos otorgo en este caso heredo los permisos de la carpeta principal.

![](media/inherited%201.png)

En este caso esta carpeta solo la podra ver juan y admin, para poder quitar los permisos heredados vamos a desplegar la opcion de opciones avanzadas y vamos a exlcuir los permisos heredados.

![](media/inherited%202.png)

Esto nos dejara los permisos de la carpeta en blanco, lo que procede ahora sera crear una regla para que el usuario juan pueda ver la carpeta, en este caso vamos al boton de crear y esto nos desplegara una cinta de opciones y usuarios, vamos a escoger el usuario que deseamos y vamos a brindarle los permisos necesarios en este caso sera de lectura y escritura completos.

![](media/inherited%203.png)

ya seleccionado los permisos que necesita le daremos en aceptar y esto guardara los cambios, es importante resaltar que el usuario admin no necesita ser registrado en estos permisos puesto que el ya tiene control total sobre los archivos y carpetas.